# DOD PKI Tools

This is a Swiss Army Knife script for installing the DoD PKE on Linux and Linux-like systems (including MacOS).

## Example Usage:

```
# Get help
dodpki -h

# Install root certificates to the system trust store
dodpki install

# Install system packages (i.e. curl, openssl, etc.) prior to adding root certificates
dodpki packages

# Install root certificates to the MacOS Login keychain
dodpki install -p darwin

# Install root certificates to the MacOS System keychain
dodpki install -p darwin -k /Library/Keychains/System.keychain

# Install root + intermediate certs
dodpki install -I

# Install the DoD WCF certificates
dodpki install -W

# Install the DoD JITC certificates
dodpki install -J

# Install the DoD ECA certificates
dodpki install -E

# Install the DoD WCF root and intermediate certificates
dodpki install -W -I

# Install root certs to the default Java keystore
dodpki install -p java

# Install root certs to a custom Java keystore
dodpki install -p java -k mykeystore.jks

# Install root certs to NSS database (i.e. Chrome, Firefox, etc.)
dodpki install -p nss

# Export the DOD PKE to ./dodpki-export
dodpki export

# Install root certs to Citrix web client (Linux)
dodpki install -p citrix

# Install from a local zip file
dodpki install -s ./Certificates.zip

# Create a PEM chain file, ./dod-ca.pem
dodpki install -p pem

# Install root + intermediates to an existing PEM chain file
dodpki install -p pem -k /etc/ssl/ca-certificates.crt -I

# Install from an alternative URL
dodpki install -s https://some.url.com/Certificates.zip
